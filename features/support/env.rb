require 'simplecov'
SimpleCov.start

ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../../spec/sate_auth/config/environment", __FILE__)
ENV["RAILS_ROOT"] ||= File.dirname(__FILE__) + '../../../spec/sate_auth'

require 'cucumber/rails'
require 'factory_bot_rails'

ActionController::Base.allow_rescue = false

begin
  DatabaseCleaner.strategy = :transaction
rescue NameError
  raise "You need to add database_cleaner to your Gemfile (in the :test group) if you wish to use it."
end

Cucumber::Rails::Database.javascript_strategy = :truncation

