require 'sate/common/methodresponse'
require 'sate/common/util'
module Sate
  module Auth
    class ApplicationController < ActionController::Base
      protect_from_forgery with: :exception
    end
  end
end
