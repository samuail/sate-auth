# Sate::Auth
This gem is common implementation to handle user account creation, role creation & assignment, defining application activities as menu items, assigining menu items to responsible role and authenticating a user based on his/her username and password.
## Usage
In order to use this gem first we have to put this statement "gem 'sate-auth'" in your project gemfile and install the gem using this statement "gem install sate_auth-0.1.0.gem" in your project directory. Then copy the migrations, create the database and run the migration to create application module, user, user role, menu and two of the join tables.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'sate-auth'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install sate-auth
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
